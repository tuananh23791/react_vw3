import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  Text,
  TextInput,
  View
} from 'react-native';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textLogin: '',
      textPassword: '',
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.background}>
          <Image style={styles.imgBackground} source={require('../image/new_background.png')}/>
        </View>
      
        <View style={styles.logo}>
          <Image style={styles.imgLogo} source={require('../image/logo.png')}/>
          <Text style={{fontSize:50, color:'white'}}>Welcome</Text>
          <Text style={styles.title}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.{"\n"}Donec autor neque sed pretium lutus</Text>
        </View>

        <View style={styles.viewInput}>
          <TextInput style={styles.input} 
                     onChangeText={(textLogin) => this.setState({textLogin})} value={this.state.textLogin} 
                     underlineColorAndroid='rgba(0,0,0,0)'
                      placeholder="Input Username"
                      placeholderTextColor = 'white'/>
        </View>

        <View style={styles.viewInput}>
          <TextInput  style={styles.input} 
                      onChangeText={(textPassword) => this.setState({textPassword})} value={this.state.textPassword}
                      underlineColorAndroid='rgba(0,0,0,0)' 
                      placeholder="Input Password"
                      placeholderTextColor = 'white'
                      secureTextEntry={true}/>
        </View>

        <View style={styles.submit}>
        <Text style={styles.textLogin}>Login</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },

  background:{
    position: 'absolute',
  },

  imgBackground: {
    flex: 1,
    resizeMode: 'contain',
  },

  logo:{
    alignItems: 'center',
    marginTop: 20,
  },

  imgLogo: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
  },

  title:{
    color:'white',
    textAlign: 'center',
    marginTop: 20,
  },

  viewInput:{
    marginRight:40,
    marginLeft:40,
    marginTop:20,
    height:40,
    paddingLeft:5,
    backgroundColor:'transparent',
    borderRadius:40,
    borderWidth: 1,
    borderColor: 'white'
  },

  input:{
    height:40,
    color:'white',
  },

  submit:{
    marginRight:40,
    marginLeft:40,
    marginTop:20,
    height:40,
    paddingLeft:5,
    backgroundColor:'blue',
    justifyContent: 'center',
    borderRadius:40,
  },

  textLogin:{
    fontSize: 17,
    color:'white',
    textAlign: 'center',
  }
});