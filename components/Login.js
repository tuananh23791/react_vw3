import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  Text,
  TextInput,
  View,
  TouchableOpacity
} from 'react-native';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textLogin: '',
      textPassword: '',
    };
  }

  checkLogin(){
      var request = new XMLHttpRequest();
    request.onreadystatechange = (e) => {
    if (request.readyState !== 4) {
      return;
    }

    if (request.status === 200) {
      console.log('success', request.responseText);
      this.props.navigator.push({name:'dashboard'})
    } else {
      console.warn('error');
    }
  };

  request.open('GET', 'http://facebook.github.io/react-native/movies.json');
  request.send();
  }

  getMoviesFromApiAsync() {
        var details = {
              'grant_type': 'password',
              'username': this.state.textLogin,
              'password': this.state.textPassword,
              'client_id': 1,
              'client_secret': 'jashdfjkh1#!$%#^2342@#$@35'
      };

      var formBody = [];
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");

      return fetch('http://103.44.65.184:3019/oauth2/token', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: formBody
      })
      .then((response) => response.json())
      .then((responseJson) => {
        var code = responseJson.code;
        console.log('success', responseJson);
        if(code != null){
        console.log('error', responseJson.error_description);
        }else{
        console.log('json', details);
        this.props.navigator.push({name:'dashboard'});
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.background}>
          <Image style={styles.imgBackground} source={require('../image/new_background.png')}/>
        </View>
      
        <View style={styles.logo}>
          <Image style={styles.imgLogo} source={require('../image/logo.png')}/>
          <Text style={{fontSize:50, color:'white'}}>Welcome</Text>
          <Text style={styles.title}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.{"\n"}Donec autor neque sed pretium lutus</Text>
        </View>

        <View style={styles.viewInput}>
          <TextInput style={styles.input} 
                     onChangeText={(textLogin) => this.setState({textLogin})} value={this.state.textLogin} 
                     underlineColorAndroid='rgba(0,0,0,0)'
                      placeholder="Input Username"
                      placeholderTextColor = 'white'/>
        </View>

        <View style={styles.viewInput}>
          <TextInput  style={styles.input} 
                      onChangeText={(textPassword) => this.setState({textPassword})} value={this.state.textPassword}
                      underlineColorAndroid='rgba(0,0,0,0)' 
                      placeholder="Input Password"
                      placeholderTextColor = 'white'
                      secureTextEntry={true}/>
        </View>

        <TouchableOpacity style={styles.submit} onPress={() => this.getMoviesFromApiAsync()}>
        <Text style={styles.textLogin}>Login</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },

  background:{
    position: 'absolute',
  },

  imgBackground: {
    flex: 1,
    resizeMode: 'contain',
  },

  logo:{
    alignItems: 'center',
    marginTop: 20,
  },

  imgLogo: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
  },

  title:{
    color:'white',
    textAlign: 'center',
    marginTop: 20,
  },

  viewInput:{
    marginRight:40,
    marginLeft:40,
    marginTop:20,
    height:40,
    paddingLeft:5,
    backgroundColor:'transparent',
    borderRadius:40,
    borderWidth: 1,
    borderColor: 'white'
  },

  input:{
    height:40,
    color:'white',
  },

  submit:{
    marginRight:40,
    marginLeft:40,
    marginTop:20,
    height:40,
    paddingLeft:5,
    backgroundColor:'blue',
    justifyContent: 'center',
    borderRadius:40,
  },

  textLogin:{
    fontSize: 17,
    color:'white',
    textAlign: 'center',
  }
});