/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import Login from './components/Login';
import Dashboard from './components/Dashboard';
import {
  AppRegistry,
  View,
  Navigator,
} from 'react-native';

class react_vw3 extends Component {
  renderScene(route, navigator){
    var globalNavigator = {navigator}

    switch(route.name){
      case "login": return( <Login {...globalNavigator}/> );
      case "dashboard": return( <Dashboard {...globalNavigator}/> );
    }
  }

  render() {
    return (
      <Navigator initialRoute={{ name: 'login' }}
                 renderScene={this.renderScene}/>
    )
  }
}

AppRegistry.registerComponent('react_vw3', () => react_vw3);
